package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();

		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
		/* El error esta en la linea 27 en el bucle �for� donde se pide que la variable �i� sea menor o igual que
		 * la longitud de la cadena. Luego en la linea 28 se pide evaluar el car�cter en la posici�n �i� y como
		 * en el charAt se empieza el primer car�cter a partir de 0 al momento de evaluar el ultimo car�cter de
		 * la cadena y no encontrar ning�n car�cter salta la excepci�n.
		 * La soluci�n seria en el bucle �for� de la cadena 27 cambiarlo a que �i� sea MENOR que la longitud de la cadena leida.
		 * Ejemplo: for (int i = 0; i < cadenaLeida.length(); i++)
		 */
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
