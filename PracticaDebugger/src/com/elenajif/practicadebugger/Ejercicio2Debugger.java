package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
		/* El error esta en el bucle �for� en la linea 20 ya que se divide el numero introducido entre 0.
		 * Esto ocurre porque en el bucle la variable �i� llega hasta que sea mayor o IGUAL que 0.
		 * La soluci�n seria que unicamente llegue �i� hasta que sea MAYOR que 0.
		 * 	Ejemplo: for(int i = numeroLeido; i > 0 ; i--)
		 */
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
				
		lector.close();
	}

}
